<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class UserController
{
    /**
     * Show a list of all of the application's users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users = User::latest()->get();
        return view('admin.users.index', compact('users'));
    }
    /**
     * Create the user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        return view('admin.users.create');
    }
    /**
     * Edit the given User.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::find(decrypt($id));
        return view('admin.users.edit', compact('user'));
    }
    /**
     * Store the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'name' => 'required', 'email' => 'required|email'
        ], ['name.required' => 'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token');
            User::create($input);
            return back()->with('message', 'Successfully Created New User');
        } else {
            return back()->withErrors($validator)->withInput(request()->all());
        }
    }
    /**
     * Update the user.
     * @param  string  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id = decrypt($request->user_id);
        $validator = Validator::make(request()->all(), [
            'name' => 'required', 'email' => 'required|email'
        ], ['name.required' => 'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token', 'image');
            User::find($id)->update($input);
            return back()->with('message', 'Successfully Updated User');
        } else {

            return back()->withErrors($validator)->withInput(request()->all());
        }
    }
    /**
     * Destroy the user data.
     * @param  string  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $id = $request->id;
        $data = User::find(decrypt($id));
        $data->delete();
        return redirect(route('admin.users'))->with('success', 'Successfully Deleted User');
    }
}
