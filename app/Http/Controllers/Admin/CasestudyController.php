<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CaseStudy;
use Validator;
use Illuminate\Support\Str;
class CasestudyController extends Controller
{
    public function index(){

        $casestudies = CaseStudy::latest()->get();
        return view('admin.casestudies.index',compact('casestudies'));

    }

    public function create()
    {

        return view('admin.casestudies.create');
    }

    public function edit($id)
    {
        $casestudy = CaseStudy::find(decrypt($id));
        return view('admin.casestudies.edit', compact('casestudy'));
    }

    public function store(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'title' => 'required','description' => 'required'
        ],['title.required'=>'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token', 'image');
            $slug = $this->craeteUniqueSlugForTable(request()->title, 'id', 0);
            $input['slug'] = $slug;
            // if (request()->hasFile('image')) {
            //     $filename = time() . "_" . Str::random(10) . '.' . request('image')->extension();
            //     request()->image->storeAs('public/casestudy/', $filename);
            //     $input['image'] = $filename;
            // }
            CaseStudy::create($input);
            return back()->with('message', 'Successfully Created New CaseStudy');
        } else {
            return back()->withErrors($validator)->withInput(request()->all());
        }
    }
    public function update(Request $request)
    {

        $id = decrypt($request->casestudy_id);
        $validator = Validator::make(request()->all(), [
            'title' => 'required', 'description' => 'required'
        ], ['title.required' => 'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token', 'image');

            // if (request()->hasFile('image')) {
            // $basicInfo = CaseStudy::find($id);

            //     if ($basicInfo->image) {
            //         $exists = Storage::disk('local')->exists('public/category/' . $basicInfo->image);

            //         if ($exists) {
            //             Storage::disk('local')->delete('public/category/' . $basicInfo->image);
            //         }
            //     }
            //     $filename = time() . "_" . Str::random(10) . '.' . request('image')->extension();
            //     request()->image->storeAs('public/category/', $filename);
            //     $input['image'] = $filename;
            // }
            CaseStudy::find($id)->update($input);
            return back()->with('message', 'Successfully Updated CaseStudy');
        } else {

            return back()->withErrors($validator)->withInput(request()->all());
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $data = CaseStudy::find(decrypt($id));

        // $exists = Storage::disk('local')->exists('public/category/' . $category->image);
        // if ($exists) {
        //     Storage::disk('local')->delete('public/category/' . $category->image);
        // }
        $data->delete();
        return redirect(route('admin.case.studies'))->with('success', 'Successfully Deleted CaseStudy');
    }
    public static function craeteUniqueSlugForTable($slug, $id_check = '', $id_value = 0)
    {
        $slug_new = Str::slug($slug);

        $query = CaseStudy::where('slug', 'LIKE', '%' . $slug_new . '%');
        if ($id_value) {
            $query->where($id_check, '!=', $id_value);
        }
        $nos = $query->count();
        if ($nos > 0) {
            $slug_new .= '-' . ($nos + 1);
        }

        return $slug_new;
    }
}
