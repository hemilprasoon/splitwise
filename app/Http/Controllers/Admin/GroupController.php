<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Expense;
use App\Models\Group;
use App\Models\User;
use App\Models\UserExpense;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class GroupController extends Controller
{
    /**
     * Show the groups for a given user.
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $groups = Group::latest()->get();
        return view('admin.groups.index', compact('groups'));
    }
    /**
     * Create the given group.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.groups.create');
    }
    /**
     * Edit the given group.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $group = Group::find(decrypt($id));
        return view('admin.groups.edit', compact('group'));
    }
    /**
     * Store the group.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make(request()->all(), [
            'title' => 'required', 'description' => 'required'
        ], ['name.required' => 'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token');
            Group::create($input);
            return back()->with('message', 'Successfully Created New Group');
        } else {
            return back()->withErrors($validator)->withInput(request()->all());
        }
    }
    /**
     * Update the group.
     * @param  string  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $id = decrypt($request->group_id);
        $validator = Validator::make(request()->all(), [
            'title' => 'required', 'description' => 'required'
        ], ['name.required' => 'Please Fill Title']);
        if ($validator->passes()) {
            $input = request()->except('_token');
            Group::find($id)->update($input);
            return back()->with('message', 'Successfully Updated Group');
        } else {

            return back()->withErrors($validator)->withInput(request()->all());
        }
    }
    /**
     * add users to  the group.
     * @param  string  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function addUsers($group_id)
    {
        $group_id = decrypt($group_id);
        $users = User::latest()->get();
        $user = new User();

        return view('admin.groups.add_user_to_group', compact('group_id', 'users'));
    }
    /**
     * Add users to  the group.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addUsersToGroup(Request $request)
    {
        $users = $request->users;
        $group = Group::find($request->group_id);
        $group->users()->sync($users);
        return back()->with('message', 'Successfully Updated Group');
    }
    /**
     * create Expense
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\View\View
     */
    public function createExpense($group_id)
    {
        $users = User::latest()->get();
        $group_id = decrypt($group_id);
        return view('admin.groups.create_expense', compact('group_id', 'users'));
    }
    /**
     * Store Expense
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeExpense(Request $request)
    {
        $users = $request->users;
        $group = Group::find($request->group_id);
        $totalAmount = $request->total_amount;
        /* creating expense */
        $expense = Expense::create([
            'title' => $request->title, 'description' => $request->description,
            'total_amount' => $totalAmount,'group_id'=> $group->id, 'user_id' => 1
        ]); // 1 for admin

        if ($expense) {
            $numberOfUsers = count($users);
            $splitAmount = ((float) $totalAmount  / (float) $numberOfUsers);
            foreach ($users as $user) {
                UserExpense::create(['expense_id' => $expense->id, 'amount' => $splitAmount, 'user_id' => $user]); // updating split amount to each users
            }
        }
        return back()->with('message', 'Successfully Updated Expense Details');
    }
    public function viewExpense($group_id)
    {
        $users = User::latest()->get();
        $group_id = decrypt($group_id);
        $group = Group::find($group_id);
        $expenses = Expense::where('group_id',$group_id)->get();
        return view('admin.groups.view_expense', compact('group', 'users','expenses'));

    }
    public function delete(Request $request)
    {
        $id = $request->id;
        $data = Group::find(decrypt($id));
        $data->delete();
        return redirect(route('admin.groups'))->with('success', 'Successfully Deleted Group');
    }

    public function viewExpenseDetails($id)
    {
        $expenseId = decrypt($id);
        $expenses = UserExpense::with('user')->where('expense_id',$expenseId)->get();
        // return $expenses;
        return view('admin.groups.view_expense_detail', compact('expenses'));

    }
}
