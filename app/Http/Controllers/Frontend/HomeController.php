<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\CaseStudy;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        return view('website.index');
    }
    public function awards()
    {

        return view('website.awards');
    }
    public function contact()
    {

        return view('website.contact');
    }
    public function faq()
    {

        return view('website.faq');
    }
    public function gallery()
    {

        return view('website.gallery');
    }
    public function media()
    {

        return view('website.media');
    }
    public function caseStudies()
    {
        $casestudies = CaseStudy::latest()->get();
        return view('website.case_studies',compact('casestudies'));
    }
    public function caseStudydetails(Request $request)
    {
        $casestudies = CaseStudy::latest()->get();
        $casestudy = CaseStudy::where('slug',$request->slug)->first();
        return view('website.case_details', compact('casestudies','casestudy'));
    }
}
