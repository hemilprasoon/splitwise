<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description','total_amount','user_id','group_id','status'];

    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
    public function group()
    {
        return $this->hasOne('App\Models\Group');
    }
}
