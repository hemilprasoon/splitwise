<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExpense extends Model
{
    use HasFactory;
    protected $fillable = ['expense_id', 'amount','user_id','status'];

    public function user()
    {
        return $this->HasOne('App\Models\User','id','user_id');
    }
}
