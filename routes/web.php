<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::group(['namespace'=>'Frontend'],function(){

//     Route::get('/', [   'uses' => 'HomeController@index'])->name('index');
//     Route::get('/media', ['uses' => 'HomeController@media'])->name('media');
//     Route::get('/contact', ['uses' => 'HomeController@contact'])->name('contact');
//     Route::get('/faq', ['uses' => 'HomeController@faq'])->name('faq');
//     Route::get('/gallery', ['uses' => 'HomeController@gallery'])->name('gallery');
//     Route::get('/awards', ['uses' => 'HomeController@awards'])->name('awards');
//     Route::get('/case-studies', ['uses' => 'HomeController@caseStudies'])->name('casestudies');
//     Route::get('/case-study/{slug}', ['uses' => 'HomeController@caseStudydetails'])->name('casestudy.details');
// });

Route::get('/manage', ['uses' => 'Admin\AdminController@login'])->name('admin.login.form');
Route::post('login', ['uses' => 'Admin\AdminController@adminLogin'])->name('admin.login');

Route::group(['prefix' => 'admin-manage', 'namespace' => 'Admin','as'=>'admin.', 'middleware' => 'isAdmin'], function () {

    Route::post('logout', ['uses' => 'AdminController@logout'])->name('logout');
    Route::get('dashboard', ['uses' => 'AdminController@dashboard'])->name('dashboard');

    Route::get('user-list', ['uses' => 'UserController@index'])->name('users');
    Route::get('user-create', ['uses' => 'UserController@create'])->name('user.create');
    Route::post('store-user', ['uses' => 'UserController@store'])->name('user.save');
    Route::get('edit-user/{id}', ['uses' => 'UserController@edit'])->name('user.edit');
    Route::post('update-user', ['uses' => 'UserController@update'])->name('user.update');
    Route::post('delete-user', ['uses' => 'UserController@delete'])->name('user.delete');

    Route::get('group-list', ['uses' => 'GroupController@index'])->name('groups');
    Route::get('group-create', ['uses' => 'GroupController@create'])->name('group.create');
    Route::post('store-groups', ['uses' => 'GroupController@store'])->name('group.save');
    Route::get('edit-groups/{id}', ['uses' => 'GroupController@edit'])->name('group.edit');
    Route::post('update-groups', ['uses' => 'GroupController@update'])->name('group.update');
    Route::post('delete-groups', ['uses' => 'GroupController@delete'])->name('group.delete');

    Route::get('add-user-to-groups/{id}', ['uses' => 'GroupController@addUsers'])->name('group.add.user');
    Route::post('add-user-to-group', ['uses' => 'GroupController@addUsersToGroup'])->name('group.add-user');
    Route::get('create-expense/{id}', ['uses' => 'GroupController@createExpense'])->name('group.expense.create');
    Route::post('store-expense', ['uses' => 'GroupController@storeExpense'])->name('store.expense');
    Route::get('view-expense/{id}', ['uses' => 'GroupController@viewExpense'])->name('group.expense.view');
    Route::get('view-bill/{id}', ['uses' => 'GroupController@viewExpenseDetails'])->name('expense.detail');


});
