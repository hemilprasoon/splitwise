# Clone The Project 

git clone https://gitlab.com/hemilprasoon/splitwise.git

Run the below  commands
1. composer instal
2.php artisan key:generate
3.php artisan migrate--seed (after connecting database in .env)

# then open project url 
http://localhost/demo/splitwise/admin-manage

enter uname:admin@gmail.com
enter passwd:123465

Explore the links from leftside navigation urls 

# contains

1. Create a user
2. Create a group
3. Add user to group
4. Add an expense to group
- **Every expense has a description, a total amount, a payer and a split among group
members in percentage or actual value**
1. List expenses of a group
2. List Total group spending, Total you paid for and your total share
3. List Balances - How much everyone in the group o

Thank You !