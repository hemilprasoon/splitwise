@extends('admin.layouts.master')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Case Study
                <small>Manage</small>
            </h1>

            @if ($message = Session::get('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fa fa-check"></i> {!! $message !!}</h5>

            </div>
            @endif
        </section>

        <section class="content">
            <div class="container-fluid">
            <div class="row">

            <div class="col-md-12">
            <div class="card card-primary">
              <div class="card-header">

                <h3 class="card-title">Create Case Study</h3>
              </div>

              <form action="{{route('admin.casestudy.save')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Title</label>
                    <input type="tezt" name="title" class="form-control" id="title" value="{{ old('title') }}" placeholder="Enter Title">
                      {!! $errors->first('title', '<p style="color:red;" class="help-block error">:message</p>') !!}
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Description</label>
                   <div class="card-body">
                        <textarea id="summernote" name="description">
                        </textarea>
            </div>
                      {!! $errors->first('description', '<p style="color:red;" class="help-block error">:message</p>') !!}
                  </div>
                  {{-- <div class="form-group">
                    <label for="exampleInputFile">Image</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="image" id="exampleInputFile">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <div class="input-group-append">
                        <span class="input-group-text">Upload</span>
                      </div>
                          {!! $errors->first('image', '<p style="color:red;" class="help-block error">:message</p>') !!}

                    </div>
                  </div> --}}

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
                   {{-- <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">

              <!-- /.card-header -->
              <!-- form start -->
              <div class="form-group">
                    <label class="col-sm-4 control-label">
                        <span data-toggle="tooltip" title="Image">{{ 'Image' }}</span></label>
                    <div class="col-sm-8">

                        @if(!empty($casestudy->image))
                        <br />
                        <img src="{{asset('storage/app/public/casestudy/')}}/{{$casestudy->image}}" />
                        @endif
                    </div>
                    <div class="col-md-12">
                        <p> * <small><b>Image format</b> - <i class="text-light-blue">allowed image
                                format
                                .jpeg,.png</i> </small></p>
                        <p> *  <small><b>Image Size</b> - <i class="text-light-blue">allowed image size
                                [256*256] pixel</i> </small></p>
                            </div>
                    </div>

            </div>

        </div> --}}
        </div>


        </section>
    </div>

@endsection
