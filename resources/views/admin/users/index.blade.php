@extends('admin.layouts.master')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                User
                <small>Manage</small>
            </h1>
        </section>

        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <div class="panel panel-default">
                        <div class="panel-heading"></div>
                        <div class="panel-body">

                            <div class="col-md-12">
                                <form method="GET" action=""
                                      accept-charset="UTF-8" class="navbar-form " role="search">
                                    <div class="row">
                                        <div class="form-group ">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" value="{{Request::get('search')}}" name="search"
                                                       placeholder="Search key">
                                            </div>
                                        </div>
                                        <div class="form-group ">
                                            <button class="btn btn-primary" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                            <a href=""     class="btn  btn-warning">Clear</a>

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <br/>
                            <br/>
                            <div class="col-md-12">
                                <a href="{{route('admin.user.create')}}"
                                   class="btn btn-success btn-sm" title="Add New ">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add New
                                </a>
                            </div>
                            <div class="table-responsive col-md-12">
                                <br/>
                                <br/>
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Id</th>
                                        <th style="width: 30%;">Name</th>
                                        <th>Email</th>
                                        <th>Action</th>
                                    </tr>
                                    @if(isset($users))

                                        @foreach($users as $user)

                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>


                                                </td>
                                                <td>
                                                   <a  href="{{route('admin.user.edit',[encrypt($user->id)])}}" class="btn btn-primary">
                                                   Edit
                                                    </a>

                                                    <form method="POST" action="{{route('admin.user.delete')}}" accept-charset="UTF-8" style="display:inline">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="id" value="{{encrypt($user->id)}}">
                                                        <button type="submit" class="btn btn-danger" title="Delete Product" onclick="return confirm('Are you sure?')">
                                                              Delete
                                                        </button>
                                                    </form>
                                                    <br/>
                                                    <br/>
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td colspan="4">No records found</td>
                                        </tr>
                                    @endif


                                </table>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
                function check() {

                    var r = confirm("Are you sure wanted to delete?");

                    if (r) {
                        return true;
                    } else {
                        return false;
                    }
                }
            </script>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@endsection
